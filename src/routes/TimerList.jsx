import React, { useState } from 'react';
import TimerWidget from '../components/TimerWidget.jsx';
import { Link } from 'react-router-dom';
import Modal from '../components/Modal.jsx';
import EditTimerForm from '../components/EditTimerForm.jsx';

export default function TimerList (props) {
    let [isFormOpen, setIsFormOpen] = useState(false);
    let close = () => {setIsFormOpen(false)};
    let open = () => {setIsFormOpen(true)};

    return (<div>
        <Modal close={close} isOpen={isFormOpen}>
            <EditTimerForm add={props.add} />
        </Modal>
        <button onClick={open}>+ add timer</button>
        {props.children}
    </div>);
}