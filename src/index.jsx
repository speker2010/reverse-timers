import ReactDom from 'react-dom';
import React, { useState } from 'react';
import TimerList from './routes/TimerList.jsx';
import store from 'store';
import TimerWidget from './components/TimerWidget.jsx';

function App () {
    let [timers, setTimers] = useState(store.get('timers') || []);
    let timerWidgets = timers.map(item => {
        function del () {
            setTimers(timers.filter(current => +current.id !== +item.id ));
        }
        function start() {
            setTimers(timers.map(current => {
                if (+current.id !== +item.id) {
                    return current;
                }
                return {
                    ...current,
                    lastStart: Date.now()
                }
            }));
        }
        function stop() {
            setTimers(timers.map(current => {
                if (+current.id !== +item.id) {
                    return current;
                }
                return {
                    ...current,
                    left: current.left + Date.now() - current.lastStart,
                    lastStart: null
                }
            }));
            item.left += Date() - item.lastStart;
        }
        return <TimerWidget key={item.id} timer={item} start={start} stop={stop} del={del} />
    });
    store.set('timers', timers);

    function nextId() {
        let maxId = timers.reduce((max, current) => {
            return Math.max(max, current.id);
        }, 0);
        return maxId + 1;
    }

    function addTimer(timer) {
        setTimers([
            ...timers,
            {
                ...timer,
                id: nextId(),
                lastStart: null,
                left: 0
            }
        ]);
    }


    return (<div>
        <TimerList  add={addTimer}>
            {timerWidgets}
        </TimerList>
    </div>);
}

ReactDom.render(<App />, document.getElementById('root'));