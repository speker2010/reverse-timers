import React, { useState, useEffect } from 'react';

export default function TimerWidget (props) {
    let [now, setNow] = useState(Date.now());
    let {name, hours, minutes, lastStart, left} = props.timer;
    let {del, start, stop} = props;
    let isStart = lastStart !== null;
    if (isStart) {
        left = left + (now - lastStart);
    }
    let seconds = hours * 60 * 60 + minutes * 60;
    let leftSeconds = Math.floor(left / 1000);
    let isOvertime = (seconds - leftSeconds) < 0;
    let leftMinutes = Math.floor((leftSeconds / 60) % 60);
    let leftHours = Math.floor(((leftSeconds/60) - leftMinutes) / (60));
    if (leftHours < 0) {
        leftHours = 0;
    }
    if (leftMinutes < 0) {
        leftMinutes = 0;
    }
    if (leftSeconds < 0) {
        leftSeconds = 0;
    }
    
    let effect = useEffect(() => {
        let timerId = setInterval(() => {setNow(Date.now())}, 1000);
        return () => {clearInterval(timerId)};
    });

    function formateTime(...time) {
        return time.map(item => item.toString().padStart(2, '0')).join(':');
    }

    let leftTime = formateTime(leftHours, leftMinutes, leftSeconds % 60);
    let time = formateTime(hours, minutes);
    
    return (<div style={isOvertime ? {color: 'red'} : {}}>
        <strong>{name}</strong> {leftTime}/{time}
        {isStart ? <button onClick={stop}>||</button> : <button onClick={start}>></button>} 
        <button onClick={del}>X</button>
    </div>);
}