import React from 'react';
import style from './modal.scss';
import ReactDOM from 'react-dom';

const modalRoot = document.getElementById('modal-portal');

export default class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.el = document.createElement('div');
    }

    componentDidMount() {
        modalRoot.appendChild(this.el);
    }

    componentWillUnmount() {
        modalRoot.removeChild(this.el);
    }

    render() {
        return ReactDOM.createPortal(
            this.props.isOpen && <div className={style.overlay} onClick={this.props.close}>
                <div className={style.modal} onClick={(e) => {e.stopPropagation()}}>
                    <div className={style.close} onClick={this.props.close}>X</div>
                    {this.props.children}
                </div>
            </div>,
            this.el
        )
    }
}