import React, { useState } from 'react';

export default function EditTimerForm (props) {
    let [name, setName] = useState('');
    let [hours, setHours] = useState(0);
    let [minutes, setMinutes] = useState(0);

    function handleName(e) {
        setName(event.target.value);
    }

    function handleHours(e) {
        setHours(event.target.value < 0 ? 0 : event.target.value);
    }

    function handleMinutes(e) {
        let value = event.target.value > 60 ? 59 : event.target.value;
        if (value < 0) {
            value = 0;
        }
        setMinutes(value);
    }

    function handleSubmit(e) {
        e.preventDefault();
        props.add({
            name,
            hours,
            minutes
        });
    }

    return (<form onSubmit={handleSubmit}>
        <div>
            Name <input type="text" value={name} onChange={handleName} placeholder='Name' />
        </div>
        <div>
            hours <input type="text" value={hours} onChange={handleHours} placeholder='hh' /> : 
            minutes <input type="text" value={minutes} onChange={handleMinutes} placeholder='mm' />
        </div>
        <input type="submit" value='add'/>
    </form>);
}